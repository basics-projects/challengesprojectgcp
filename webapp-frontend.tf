resource "google_cloud_run_service" "frontend-kpmg" {
  name     = "${var.app_name}-frontend-kpmg"
  location = var.region
  project  = var.project_id

  template {
    spec {
      service_account_name = google_service_account.runsa.email
      containers {
        image = local.fe_image
        ports {
          container_port = 80
        }
        env {
          name  = "ENDPOINT"
          value = google_cloud_run_service.middleware-kpmg.status[0].url
        }
      }
    }
  }
  metadata {
    labels = var.labels
  }
}
