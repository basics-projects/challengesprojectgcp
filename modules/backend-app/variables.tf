
#-----------------

variable "app_name" {
  type        = string
  description = "Application name"
}

variable "project_id" {
  type        = string
  description = "Project ID"
}

variable "region" {
  type        = string
  description = "Region"
}

variable "labels" {
  type        = map(string)
  description = "Labels for resources"
}

variable "run_service_account_email" {
  type        = string
  description = "Email of the service account for Cloud Run"
}

variable "api_image" {
  type        = string
  description = "API container image URL"
}

variable "redis_instance_host" {
  type        = string
  description = "Redis instance host"
}

variable "sql_instance_ip_address" {
  type        = string
  description = "IP address of the SQL instance"
}

variable "sql_instance_connection_name" {
  type        = string
  description = "Connection name of the SQL instance"
}

variable "vpc_access_connector_id" {
  type        = string
  description = "VPC access connector ID"
}
