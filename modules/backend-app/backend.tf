resource "google_cloud_run_service" "middleware-kpmg" {
  name     = "${var.app_name}-middleware-kpmg"
  provider = google-beta
  location = var.region
  project  = var.project_id

  template {
    spec {
      service_account_name = var.run_service_account_email
      containers {
        image =  var.api_image
        env {
          name  = "redis_host"
          value = var.redis_instance_host
        }
        env {
          name  = "db_host"
          value = var.sql_instance_ip_address
        }
        env {
          name  = "db_user"
          value = var.run_service_account_email
        }
        env {
          name  = "db_conn"
          value = var.sql_instance_connection_name
        }
        env {
          name  = "db_name"
          value = "dbbackend"
        }
        env {
          name  = "redis_port"
          value = "6379"
        }

      }
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale"        = "30"
        "run.googleapis.com/cloudsql-instances"   = var.sql_instance_connection_name
        "run.googleapis.com/client-name"          = "terraform"
        "run.googleapis.com/vpc-access-egress"    = "all"
        "run.googleapis.com/vpc-access-connector" = var.vpc_access_connector_id

      }
    }
  }
  metadata {
    labels = var.labels
  }
  autogenerate_revision_name = true
  #depends_on = [
  #  google_sql_user.main,
  #  google_sql_database.database
  #]
}