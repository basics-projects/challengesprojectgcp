
/*
variable "app_name" {
  type        = string
  description = "apps name added as prefix."
  default     = "three-tier-app-gcp-terraform"
}
variable "region" {
  type        = string
  description = "Region"
}
variable "project_id" {
  type        = string
  description = "project ID"
}
variable "zone" {
  type        = string
  description = "Zone"
}
variable "labels" {
  type        = map(string)
  description = "A map of labels for resources."
  default     = { "three-tier-app-gcp-terraform" = true }
}

*/

variable "project_id" {
  type        = string
  description = "project ID"
}

variable "region" {
  type        = string
  description = "Compute Region"
}

variable "zone" {
  type        = string
  description = "Compute Zone"
}

variable "app_name" {
  type        = string
  description = "prefix for app."
  #default     = "todo-list"
  default     = "tier-app"
}

variable "labels" {
  type        = map(string)
  description = "map of labels."
  default     = { "tier-kpmg-app" = true }
}
#--------------------------
variable "network_name" {
  type        = string
  description = "Compute network name"
}

variable "network_self_link" {
  type        = string
  description = "Compute network self link"
}

variable "run_service_account_email" {
  type        = string
  description = "Email of the Cloud Run service account"
}
