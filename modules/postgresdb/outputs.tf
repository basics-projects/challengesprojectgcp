output "database_instance_name" {
  value       = google_sql_database_instance.main.name
  description = "Database instance name."
}

output "database_name" {
  value       = google_sql_database.database.name
  description = "Database name."
}

#----------

output "redis_host" {
  value       = google_redis_instance.main.host
  description = "Redis instance host"
}

output "sql_instance_ip_address" {
  value       = google_sql_database_instance.main.ip_address[0].ip_address
  description = "IP address of the SQL instance"
}

output "sql_instance_connection_name" {
  value       = google_sql_database_instance.main.connection_name
  description = "Connection name of the SQL instance"
}
