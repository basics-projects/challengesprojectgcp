output "network_name" {
  value       = google_compute_network.main.name
  description = "Compute network name."
}

output "network_self_link" {
  value       = google_compute_network.main.self_link
  description = "Compute network self link."
}

#--------

output "vpc_access_connector_id" {
  value       = google_vpc_access_connector.main.id
  description = "VPC access connector ID"
}
