variable "project_id" {
  type        = string
  description = "project ID"
}

variable "app_name" {
  type        = string
  description = "prefix for app."
  #default     = "todo-list"
  default     = "tier-app"
}

variable "run_roles" {
  description = "roles used by cloud run service"
  type        = list(string)
  default = [
    "roles/cloudsql.instanceUser",
    "roles/cloudsql.client",
  ]
}


#--------------------------

variable "middleware_kpmg_location" {
  description = "Middleware KPMG Cloud Run Service location"
  type        = string
}

variable "middleware_kpmg_project" {
  description = "Middleware KPMG Cloud Run Service project"
  type        = string
}

variable "middleware_kpmg_name" {
  description = "Middleware KPMG Cloud Run Service name"
  type        = string
}

variable "frontend_kpmg_location" {
  description = "Frontend KPMG Cloud Run Service location"
  type        = string
}

variable "frontend_kpmg_project" {
  description = "Frontend KPMG Cloud Run Service project"
  type        = string
}

variable "frontend_kpmg_name" {
  description = "Frontend KPMG Cloud Run Service name"
  type        = string
}
