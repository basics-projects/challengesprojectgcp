# Fetch project details
data "google_project" "project_kpmg" {
  project_id = var.project_id
}

# Define local variables for container image references
locals {
  api_image = "us-central1-docker.pkg.dev/test-prov2-381004/wacelas-repo-public/api_img_kpmg_v2:tag1"
  fe_image  = "us-central1-docker.pkg.dev/test-prov2-381004/wacelas-repo-public/front_img_kpmg:tag1"
}

# Enable required services for the project
module "enable_services" {
  source      = "./modules/enable-services"
  project_id  = var.project_id
  enable_apis = var.enable_apis
}

# Create service accounts for the applications
module "service_accounts" {
  source                  = "./modules/service-accounts"
  project_id              = var.project_id
  app_name                = var.app_name
  run_roles               = var.run_roles
  middleware_kpmg_location = google_cloud_run_service.middleware-kpmg.location
  middleware_kpmg_project = google_cloud_run_service.middleware-kpmg.project
  middleware_kpmg_name = google_cloud_run_service.middleware-kpmg.name
  frontend_kpmg_location = google_cloud_run_service.frontend-kpmg.location
  frontend_kpmg_project = google_cloud_run_service.frontend-kpmg.project
  frontend_kpmg_name = google_cloud_run_service.frontend-kpmg.name
}


# Set up the Postgres database instance
module "postgresdb" {
  source            = "./modules/postgresdb"
  project_id        = var.project_id
  region            = var.region
  zone              = var.zone
  app_name          = var.app_name
  labels            = var.labels
  #run_roles         = var.run_roles
  network_name      = module.networks.network_name
  network_self_link = module.networks.network_self_link
  run_service_account_email = module.service_accounts.run_service_account_email
}

# Create the network resources required for the project
module "networks" {
  source     = "./modules/networks"
  project_id = var.project_id
  region     = var.region
  app_name   = var.app_name
}

# Deploy the backend application on Cloud Run
module "backend_app" {
  source                   = "./modules/backend-app"
  app_name                 = var.app_name
  project_id               = var.project_id
  region                   = var.region
  labels                   = var.labels
  run_service_account_email = module.service_accounts.run_service_account_email
  api_image                = local.api_image
  redis_instance_host      = module.postgresdb.redis_host
  sql_instance_ip_address  = module.postgresdb.sql_instance_ip_address
  sql_instance_connection_name = module.postgresdb.sql_instance_connection_name
  vpc_access_connector_id  = module.networks.vpc_access_connector_id
}
