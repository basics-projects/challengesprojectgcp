# import json

# def get_value_from_nested_object(obj, key):
#     keys = key.split('/')
#     val = obj
#     for k in keys:
#         if k in val:
#             val = val[k]
#         else:
#             return None
#     return val

# if __name__ == '__main__':
#     obj_str = input('Enter the nested object in JSON format as {"a":{"b":{"c":"d"}}} : ')
#     obj = json.loads(obj_str)
#     key = input('Enter the key in the format of a/b/c: ')

#     result = get_value_from_nested_object(obj, key)
#     print(result)

#----------------version 1 --------------------#

# def get_nested_value(nested_dict, key):
#     keys = key.split('/')
#     value = nested_dict
#     for k in keys:
#         if k in value:
#             value = value[k]
#         else:
#             return None
#     return value

# # Example usage
# object1 = {"a": {"b": {"c": "d"}}}
# key1 = "a/b/c"

# object2 = {"x": {"y": {"z": "a"}}}
# key2 = "x/y/z"

# print(get_nested_value(object1, key1))  # Output: d
# print(get_nested_value(object2, key2))  # Output: a


#---------------------V2---------------------

# Function to retrieve the nested value based on the given key
def get_nested_value(nested_dict, key):
    keys = key.split('/')  # Split the key into a list of individual keys
    value = nested_dict  # Set the initial value to the nested dictionary
    for k in keys:  # Iterate through the individual keys
        if k in value:  # Check if the key exists in the current dictionary level
            value = value[k]  # Update the value to the next dictionary level
        else:
            return None  # If the key does not exist, return None
    return value  # Return the final value found in the nested dictionary

# Function to test the get_nested_value function
def test_get_nested_value():
    # Test case 1
    object1 = {"a": {"b": {"c": "d"}}}
    key1 = "a/b/c"

    # Test case 2
    object2 = {"x": {"y": {"z": "a"}}}
    key2 = "x/y/z"

    # Test case 3
    object3 = {"a": {"b": {"c": "d"}}}
    key3 = "a/b/e"

    try:
        # Test assertions to verify the correctness of the get_nested_value function
        assert get_nested_value(object1, key1) == "d"
        assert get_nested_value(object2, key2) == "a"
        assert get_nested_value(object3, key3) is None

        # If all tests pass, print a success message
        print("All tests passed!")

    # Catch any AssertionError exceptions
    except AssertionError as e:
        import traceback
        traceback.print_exc()  # Print a traceback of the exception
        # Print a custom error message to inform the user about the failed test
        print("A test has failed. Please check the test conditions and the function implementation.")

# Execute the test function
test_get_nested_value()
