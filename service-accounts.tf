resource "google_service_account" "runsa" {
  project      = var.project_id
  account_id   = "${var.app_name}-run-sa"
  display_name = "Service Account for Cloud Run"
}

resource "google_project_iam_member" "allrun" {
  for_each = toset(var.run_roles)
  project  = data.google_project.project.number
  role     = each.key
  member   = "serviceAccount:${google_service_account.runsa.email}"
}

resource "google_cloud_run_service_iam_member" "noauth_middleware-kpmg" {
  location = google_cloud_run_service.middleware-kpmg.location
  project  = google_cloud_run_service.middleware-kpmg.project
  service  = google_cloud_run_service.middleware-kpmg.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}

resource "google_cloud_run_service_iam_member" "noauth_frontend-kpmg" {
  location = google_cloud_run_service.frontend-kpmg.location
  project  = google_cloud_run_service.frontend-kpmg.project
  service  = google_cloud_run_service.frontend-kpmg.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}
