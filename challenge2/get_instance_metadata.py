
# import requests
# import json

# def get_instance_metadata(key):
#     metadata_url = f'http://metadata.google.internal/computeMetadata/v1/instance/{key}'
#     headers = {'Metadata-Flavor': 'Google'}
#     response = requests.get(metadata_url, headers=headers)

#     if response.status_code == 200:
#         metadata = {key: response.text}
#     else:
#         metadata_url = 'http://metadata.google.internal/computeMetadata/v1/instance/?recursive=true'
#         response = requests.get(metadata_url, headers=headers)

#         metadata = {}
#         for line in response.text.splitlines():
#             k, v = line.split(':', 1)
#             metadata[k] = v.strip()

#     return json.dumps(metadata)

# if __name__ == '__main__':
#     key = input("Enter the key name to retrieve: ")
#     result = get_instance_metadata(key)
#     print(result)


#-------------------AWS get-instance-metadata--------------------------#


# Import the required libraries
import requests
import json

# Define a function to get the instance metadata
def get_instance_metadata(key=None):
    # Set the base URL for the AWS metadata service
    metadata_url = 'http://169.254.169.254/latest/meta-data/'
    
    # If a key is provided, append it to the base URL
    if key:
        metadata_url += key

    # Attempt to fetch the metadata from the specified URL
    try:
        response = requests.get(metadata_url, timeout=1)

        # If the request is successful (status code 200)
        if response.status_code == 200:
            # If a key is provided, return the metadata for that key
            if key:
                metadata = {key: response.text}
            # Otherwise, iterate through all the metadata keys and fetch their values
            else:
                metadata = {}
                for line in response.text.splitlines():
                    sub_url = metadata_url + line
                    sub_response = requests.get(sub_url)
                    metadata[line] = sub_response.text
        # If the request is not successful, return an error message
        else:
            metadata = {"error": "Could not fetch metadata"}

    # If there's an exception while fetching the metadata, return an error message
    except requests.exceptions.RequestException:
        metadata = {"error": "Could not connect to metadata endpoint"}

    # Return the metadata as a JSON-formatted string
    return json.dumps(metadata)

# Define the main function to execute when the script is run
if __name__ == '__main__':
    # Prompt the user for a key name to retrieve
    key = input("Enter the key name to retrieve (leave empty for all metadata): ").strip() or None

    # Call the get_instance_metadata function with the provided key
    result = get_instance_metadata(key)

    # Print the JSON-formatted metadata
    print(result)
