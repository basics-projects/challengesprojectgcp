# William Acelas Cloud Run Project Deployment

This project deploys a multi-tier web application using Google Cloud Run, Google Cloud SQL, and Redis. The application consists of a frontend, middleware, and backend components. This repository contains Terraform configuration files and modules to deploy the infrastructure and services required for the application.

## Prerequisites

- A Google Cloud Platform (GCP) account with billing enabled.
- The [Google Cloud SDK](https://cloud.google.com/sdk/docs/install) installed and configured with `gcloud` CLI.
- Terraform installed (version 1.0.11 or later).
- To have a bucket created to sabe the *.tfvars file there. (check the providers.tf file)

## Deployment Steps

Follow these steps to deploy the infrastructure and services for the KPMG Cloud Run project.

1. **Clone the repository**:

```python
git clone https://gitlab.com/your-username/kpmg-cloud-run-project.git
cd kpmg-cloud-run-project
```
2. **Authenticate with Google Cloud Platform**:

Run the following command and follow the prompts to authenticate with your GCP account:

```python
gcloud auth application-default login
```

3. **Set environment variables**:

```python
export TF_VAR_project_id="your-project-id"
export TF_VAR_region="your-region"
```
Optionally, you can create a `.env` file in the project root to store these variables and use a tool like [direnv](https://direnv.net/) to load them automatically.

4. **Initialize Terraform**:

```python
terraform init
```

This command downloads the required provider plugins and sets up the backend for storing the Terraform state.

5. **Review the Terraform plan**:

```python
terraform plan
```

This command shows the resources that will be created, modified, or destroyed. Review the output to ensure the configuration is correct.

6. **Apply the Terraform configuration**:

```python
terraform apply
```

This command creates the infrastructure and services defined in the Terraform configuration. Confirm by typing `yes` when prompted.

7. **Verify the deployment**:

After the `terraform apply` command completes, you can view the created resources in the [Google Cloud Console](https://console.cloud.google.com/).

## Cleaning Up

To destroy the created resources and prevent further costs, run the following command:

```python
terraform destroy
```


Confirm by typing `yes` when prompted. This command removes all resources created by the Terraform configuration.

## Further Reading

- [Terraform Documentation](https://www.terraform.io/docs/index.html)
- [Google Cloud Run Documentation](https://cloud.google.com/run/docs)
- [Google Cloud SQL Documentation](https://cloud.google.com/sql/docs)
- [Google Cloud Redis Documentation](https://cloud.google.com/memorystore/docs/redis)
- [Google Cloud Platform repo](https://github.com/GoogleCloudPlatform)
- [Google Cloud Guides - Three Tier App](https://cloud.google.com/shell/docs/cloud-shell-tutorials/deploystack/three-tier-app)
- [GCP DevOps-Infra — Three Tier Architecture Deployment using Terraform](https://medium.com/google-cloud/gcp-devops-infra-three-tier-architecture-deployment-using-terraform-4ef78faed444)

## License

This project is licensed under the [MIT License](LICENSE).

## Next Steps for this project

- Finish to modularized all the modules (frontend module missing)
- Add code documentation (at least comments the code line by line)
- Add Google Cloud Build as a CI/CD.
- Apply best practices, security, images modifications.
- Add another elements like a CDN, LB and other interesting stuff.
- Try with Tekton.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)


## Challenge 2

- [Retrieve instance metadata - Amazon Elastic Compute Cloud](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html)
- [ProgramCreek - Get instance metadata](https://www.programcreek.com/python/?CodeExample=get+instance+metadata)
- [Get EC2's region from instance metadata using Python](https://gist.github.com/doublenns/7e3e4b72df4aaeccbeabf87ba767f44e)


## Challenge 3

- [StackOverflow](https://stackoverflow.com/questions/48550533/accessing-nested-objects-with-python)
- [Working with large nested JSON Data](https://ankushkunwar7777.medium.com/get-data-from-large-nested-json-file-cf1146aa8c9e)

